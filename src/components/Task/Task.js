import React, { Component, useRef, useState } from 'react';
import { FormattedMessage } from 'react-intl';
import { Row, Col, Button } from 'react-bootstrap';
import ms from 'pretty-ms';



export default class Task extends Component {
    constructor(props) {
        super(props);
        this.state = {
            time: this.props.data.time,
            isOn: this.props.data.isOn,
            start: this.props.data.start,
            title: this.props.data.title,
            editing: false
        }

        this.startTimer = this.startTimer.bind(this);
        this.stopTimer = this.stopTimer.bind(this);
        this.resetTimer = this.resetTimer.bind(this);
        this.edit = this.edit.bind(this);
        this.save = this.save.bind(this);
        this.rendering = this.rendering.bind(this);
        this.editFunctions = this.editFunctions.bind(this);
    }



    edit() {
        this.setState({
            editing: true
        });
    }

    save() {
        var val = this.refs.newText.value;
        this.setState({
            title: val,
            editing: false
        });
        this.props.pass(val, this.state.time, this.state.start, this.state.isOn, this.props.index);
    }

    startTimer() {
        this.setState({
            isOn: true,
            time: this.state.time,
            start: Date.now() - this.state.time
        })
        this.timer = setInterval(() => this.setState({
            time: Date.now() - this.state.start
        }), 1);
    }

    stopTimer() {
        this.setState({ isOn: false });
        clearInterval(this.timer);
        this.props.pass(this.state.title, this.state.time, this.state.start, this.state.isOn, this.props.index);
    }

    rendering() {
        let renderNormal = <div>
            <h2>{this.state.title}</h2>
        </div>
        let renderForm = <div>
            <textarea className="form-control" ref="newText" defaultValue={this.state.title}></textarea>
        </div>

        if (this.state.editing) {
            return renderForm
        } else {
            return renderNormal
        }
    }

    editFunctions(){
        let one = <Button bsPrefix="btn-sec"  onClick={this.edit}><FormattedMessage id="Edit"/></Button>
        let two = <Button bsPrefix="btn-sec" onClick={this.save}><FormattedMessage id="Save"/></Button>
        if (this.state.editing){
            return two;
        }
        else{
            return one;
        }
    }

    resetTimer() {
        this.setState({ time: 0, isOn: false })
    }

    componentDidMount() {
    }

    render() {
        let start = (this.state.time === 0) ? <Button id="sbutton" bsPrefix="pbutton" className="mr-2 my-2" onClick={this.startTimer}><FormattedMessage id="Start"/></Button> : null
        let stop = (this.state.time === 0 || !this.state.isOn) ? null : <Button id="hbutton" bsPrefix="pbutton" className="mr-2 my-2" onClick={this.stopTimer}><FormattedMessage id="Stop"/></Button>
        let resume = (this.state.time === 0 || this.state.isOn) ? null : <Button id="rbutton" bsPrefix="pbutton" className="mr-2 my-2" onClick={this.startTimer}><FormattedMessage id="Resume"/></Button>
        let reset = (this.state.time === 0 || this.state.isOn) ? null : <Button id="bbutton" bsPrefix="pbutton" className="mr-2 my-2" onClick={this.resetTimer}><FormattedMessage id="Reset"/></Button>

        return (
            <Row style={{border: '1px dashed #0C1C19'}} className="align-items-center">
                <Col>
                <h2>{this.rendering()}</h2>
                <p id="Tt"><FormattedMessage id="Timer"/>: {ms(this.state.time)}</p>
                {start}
                {resume}
                {stop}
                {reset}
                </Col>
                <Col sm={2}>
                {this.editFunctions()}
                </Col>
            </Row>
        )
    }
}