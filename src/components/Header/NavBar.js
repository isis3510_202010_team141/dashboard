import React from 'react';
import { Link, withRouter } from 'react-router-dom';
import './NavBar.css';
import Navbar from 'react-bootstrap/Navbar';


const NavBar = (props) => {

    return (
        <div className="header">
            {/* Logo */}
            <Link className="nav-title" to="/">
                <Navbar.Brand >
                    <h1>Finance Analytics</h1>
                </Navbar.Brand>
            </Link>


            {/* Page Links */}
            <div className="nav-items">
                <Link className="nav-link pbutton" to='/Home'>Home</Link>
            </div>

        </div>
    )
};

export default withRouter(NavBar);
