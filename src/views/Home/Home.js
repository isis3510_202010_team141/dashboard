import React, { useRef, useState } from "react";
import { Container, Row, Col, ButtonGroup, ToggleButton, InputGroup } from "react-bootstrap";
import { HorizontalBar, Bar, Line } from '@reactchartjs/react-chart.js';
import DatePicker from 'react-datepicker';
import "react-datepicker/dist/react-datepicker.css";
import * as d3 from "d3";
import axios from "axios";
import './Home.css'

const Home = (props) => {
    const inputEl = useRef("canvas");
    const [radioValue, setRadioValue] = useState(0);
    const [currentData, setCurrentData] = useState({});
    const [tempData, setTempData] = useState({});
    const [startDate, setStartDate] = useState(new Date());
    const [endDate, setEndDate] = useState(new Date());
    const url = "https://backend-analytics-flask.herokuapp.com/";
    const djangoURL = "https://django-middle-finance.herokuapp.com/"

    const radios = [
        { name: "OS", value: 0 },
        { name: "Function Respond Time", value: 1 },
        { name: "Function Frequency", value: 2 },
        { name: "Theme Frequency", value: 3 },
        { name: "Users cash flow %", value: 4},
        { name: "Users cash flow avg"}
    ]

  

    const checkHandler = (value) => {
        setRadioValue(value);
        let header = "";
        switch (parseInt(value)) {
            case 0:
                header = "os"
                axios.get(url + header).then(resp => resp.data)
                    .then(data => {drawOS(data)})
                    .catch(err => alert(err.message));
                break;
            case 1:
                console.log("Function time selected");
                break;
            case 2:
                header = "requests/"
                axios.get(djangoURL + header).then(resp => resp.data)
                    .then(data => {drawFunFrequency(data)})
                    .catch(err => alert(err.message));
                break;
            case 3:
                header = "usrthemepref";
                axios.get(url + header).then(resp => resp.data)
                    .then(data => {drawThemePrefs(data)})
                    .catch(err => alert(err.message));
                break;
            case 4:
                header = "transactions/"
                axios.get(djangoURL + header).then(resp=> resp.data)
                    .then(data => {drawUserCashFlow(data)})
                    .catch(err => alert(err.message));
                break;
        }
    }

    const drawUserCashFlow = (pData) =>{
        d3.select("svg").remove();
        let dataSet = {"clients":[], "totals":[]}
        pData.forEach(d =>{
            d.client = +d.client;
            d.value = +d.value;
            let currentDate = new Date(d.date);
            const index = dataSet.clients.indexOf(d.client);
            if (currentDate >= startDate && currentDate <= endDate){
                if(index !== -1){
                    dataSet.totals[index] += !d.typeOf? d.value : d.value * -1;
                }
                else{
                    dataSet.clients.push(d.client);
                    dataSet.totals.push(!d.typeOf? d.value: d.value * -1);
                }
            }
        })
        let surplus = 0;
        let revenueAverage = 0;
        let costAverage = 0;
        for (let index = 0; index < dataSet.clients.length; index++) {
            const total = dataSet.totals[index];
            if(total > 0) {
                surplus += 1;
                revenueAverage += total;
            }
            else costAverage += total;

        }
        let percent = surplus > 0? surplus/dataSet.clients.length : 0;
        revenueAverage = revenueAverage > 0? revenueAverage/dataSet.clients.length: 0;
        costAverage = costAverage > 0 ? costAverage / dataSet.clients.length : 0;
        let data = {
            labels: ['In surplus', 'In debt'],
            datasets: [
                {
                    type: 'bar',
                    label: '% of Users that have spent more and viceversa',
                    data: [percent, 1-percent],
                    backgroundColor: ['rgba(51, 204, 51, 0.3)', 'rgb(204, 51, 51, 0.3)'],
                    borderColor: ['rgba(0, 0, 0, 1)', 'rgba(0, 0, 0, 1)'],
                    borderWidth: 1,
                },
            ],
        }
        let data2 = {
            labels: ['In surplus', 'In debt'],
            datasets: [
                {
                    label: 'Averages',
                    data: [revenueAverage, costAverage],
                    borderColor: 'rgb(54, 162, 235, 0.4)',
                    borderWidth: 2,
                    fill: false,
                }   
            ],
        }
        setTempData(data2);
        setCurrentData(data);
    }
/*
      "id": 6,
      "client": 1,
      "name": "Almuerzo",
      "value": "25100.330",
      "typeOf": false,
      "date": "2020-10-18T23:38:59.324521Z"
  */
    const drawThemePrefs = (pData) =>{
        d3.select("svg").remove();

        let dic = { "black": 0, "white": 0 }
        pData.forEach(d =>{
            d.theme = +d.theme;
            if(d.theme === 1){
                dic.black += 1;
            }
            else{
                dic.white += 1;
            }
        })
        let data = {
            labels: ['Black', 'White'],
            datasets: [
                {
                    label: '# of Users that use dark or light theme',
                    data: [dic.black, dic.white],
                    backgroundColor: ['rgba(0, 0, 0, 0.2)', 'rgba(255, 255, 255, 0.4)'],
                    borderColor: ['rgba(0, 0, 0, 1)', 'rgba(0, 0, 0, 1)'],
                    borderWidth: 1,
                },
            ],
        }
        setCurrentData(data);
    }

    const drawOS = (data) => {
        d3.select("svg").remove();


        const canvas = d3.select(inputEl.current);
        let margin = { top: 20, right: 20, bottom: 70, left: 40 },
            width = 800 - margin.left - margin.right,
            height = 500 - margin.top - margin.bottom;


        let x = d3.scaleBand().rangeRound([0, width]).padding(.05)
        let y = d3.scaleLinear().range([height, 0])

        let svg = canvas
            .append("svg")
            .attr("width", width + margin.left + margin.right)
            .attr("height", height + margin.top + margin.bottom)
            .append("g")
            .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

        let newData = []

        data.forEach(d => {
            var currentOS = d.os_version
            var has = false
            newData.forEach(elem => {
                if(elem.os != null && elem.os === currentOS){
                    elem.count += 1
                    has = true
                }
            })
            if(!has){
                newData.push({"os":d.os_version, "count":1})
            }

        });
        x.domain(newData.map(function (d) { return d.os }))
        y.domain([0, d3.max(newData, function (d) { return d.count })])

        svg.selectAll(".bar")
            .data(newData)
            .enter().append("rect")
            .attr("class", "bar")
            .attr("x", function (d) { return x(d.os) })
            .attr("width", x.bandwidth())
            .attr("y", function (d) { return y(d.count) })
            .attr("height", function (d) { return height - y(d.count) })

        svg.append("g")
            .attr("transform", "translate(0," + height + ")")
            .call(d3.axisBottom(x));

        svg.append("g")
            .call(d3.axisLeft(y))

    }

    const drawFunFrequency = (data) =>{
        d3.select("svg").remove();


        const canvas = d3.select(inputEl.current);
        let margin = { top: 20, right: 20, bottom: 70, left: 40 },
            width = 800 - margin.left - margin.right,
            height = 500 - margin.top - margin.bottom;


        let x = d3.scaleBand().rangeRound([0, width]).padding(.05)
        let y = d3.scaleLinear().range([height, 0])

        let svg = canvas
            .append("svg")
            .attr("width", width + margin.left + margin.right)
            .attr("height", height + margin.top + margin.bottom)
            .append("g")
            .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

      
        x.domain(data.map(function (d) { return d.name }))
        y.domain([0 , d3.max(data, function(d){return d.amount})])

        svg.selectAll(".bar")
            .data(data)
            .enter().append("rect")
            .attr("class", "bar")
            .attr("x", function (d) { return x(d.name)})
            .attr("width", x.bandwidth())
            .attr("y", function (d) { return y(d.amount) }) 
            .attr("height", function (d) { return height - y(d.amount) }) 

        svg.append("g")
            .attr("transform", "translate(0," + height + ")")
            .call(d3.axisBottom(x));

        svg.append("g")
            .call(d3.axisLeft(y))
    }

    const options = {
        scales: {
            yAxes: [
            {
                ticks: {
                beginAtZero: true,
                },
            },
            ],
        },
    }


    return (
        <Container className="master">
            <Row>
                <Col xs sm md lg="2">
                    <ButtonGroup vertical toggle>
                        {radios.map((e, i) => (
                            <ToggleButton
                                key={i}
                                type="radio"
                                name="radio"
                                variant="secondary"
                                value={e.value}
                                checked={radioValue === e.value}
                                onChange={element => checkHandler(element.currentTarget.value)}
                            >
                                {e.name}
                            </ToggleButton>
                        ))}
                    </ButtonGroup>
                    <InputGroup>
                        <InputGroup.Prepend>
                            <InputGroup.Text id="btnGroupAddon">Start Date</InputGroup.Text>
                        </InputGroup.Prepend>
                        <DatePicker selected={startDate} onChange={date => setStartDate(date)} />
                        <InputGroup.Prepend>
                            <InputGroup.Text id="btnGroupAddon2">End Date</InputGroup.Text>
                        </InputGroup.Prepend>
                        <DatePicker selected={endDate} onChange={date => setEndDate(date)} />
                    </InputGroup>
                    
                </Col>
                <Col>
                    <div ref={inputEl}>

                    </div>
                    {
                        parseInt(radioValue) === 3 ?
                            <HorizontalBar data={currentData} options={options} /> :
                            parseInt(radioValue) === 4 ?
                                <div>
                                    <Bar data={currentData} />
                                </div>:
                                parseInt(radioValue) === 5 ?
                                <Line data={tempData} options={options} />:
                                null
                            
                    }
                </Col>
            </Row>
            <Row>
                
            </Row>
        </Container>
    )
}

export default Home;