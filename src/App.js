import React from 'react';
import {Route, Switch, Redirect} from 'react-router-dom';
import './App.css';
import NavBar from './components/Header/NavBar';
import Home from './views/Home/Home';
import NotFound from './views/NotFound';
const App = () => {


  
  return (
    <div className="taskbg">
      <NavBar />
      <Switch>
        <Route exact path="/Home" render={(props) => <Home {...props} />} />
        <Route exact path="/">
          <Redirect to="/Home" />
        </Route>
        <Route component={NotFound} />
      </Switch>
    </div>
  );
}

export default App;
